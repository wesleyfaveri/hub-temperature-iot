import registerApi from 'api';
import { registerDb } from 'db';

export const registerSdk = (app, apiPath = '/api') => {
  app.use(apiPath, registerApi());

  registerDb();

  return app;
};

export default registerSdk;
