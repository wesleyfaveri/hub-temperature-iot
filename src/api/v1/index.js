import express from 'express';
import temperature from './temperature';

const app = express();

app.use(temperature);

export default app;
