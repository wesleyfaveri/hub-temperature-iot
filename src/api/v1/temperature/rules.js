export const verifyFields = ({ idUser, latitude, longitude, temperature }) => {
  if (isNotValid(idUser)) {
    return 'idUser missing';
  }

  if (isNotValid(latitude)) {
    return 'Latitude missing';
  }

  if (isNotValid(longitude)) {
    return 'Longitude missing';
  }

  if (isNotValid(temperature)) {
    return 'Temperature missing';
  }

  return '';
};

const isNotValid = (value) => value ? false : true;

export const getDistinct = (values) => {
  return [...new Set(values.map((item) => item.idUser))];
};

export const getValueById = (values, idUsers) => {

  return idUsers.map((idUser) => {
    const itemsUser = values.filter((item) => item.idUser === idUser);

    const mostRecentValue = getMostRecentValue(itemsUser);

    return mostRecentValue;
  });
};

const getMostRecentValue = (list) => list.reduce((prev, current) => (prev.date > current.date) ? prev : current);
