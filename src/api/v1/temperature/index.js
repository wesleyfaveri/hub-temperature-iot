import express from 'express';
import { get, getList, getListUser, getListAll } from './temperature';

const app = express();
const router = express.Router();

router.get('/temperature/:idUser/:latitude/:longitude/:temperature/:humidity/:luminosity', get);

router.get('/temperature/list', getList);
router.get('/temperature/list/:idUser', getListUser);
router.get('/temperature/listAll', getListAll);

app.use('/', router);

export default app;
