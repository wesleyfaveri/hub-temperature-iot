import { Temperature } from 'db';
import { v4 as uuidv4 } from 'uuid';
import { verifyFields, getDistinct, getValueById } from './rules';
import moment from 'moment';

const get = (req, res) => {
  const { params } = req;

  const result = verifyFields(params);

  if ((result) && (result.length > 0)) {
    return res.status(500).json({ message: result });
  } else {
    const uuid = uuidv4();

    const item = {
      ...params,
      uuid,
      date: moment().valueOf(),
    };

    return Temperature.create(item).then(() => res.json({ result: 'Show de bola' }));
  }
};

const getList = (req, res) => {
  return Temperature.find().then((items) => {
    const idUsers = getDistinct(items);

    const values = getValueById(items, idUsers);

    return res.json({ result: values });
  });
};

const getListUser = (req, res) => {
  const { params } = req;
  return Temperature.find(params).then((values) => {
    return res.json({ result: values });
  });
};

const getListAll = (req, res) => Temperature.find().then((values) => res.json({ result: values }));

export {
  get,
  getList,
  getListUser,
  getListAll,
};
