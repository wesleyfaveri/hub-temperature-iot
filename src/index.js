import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import sdk from './sdk';
import methodOverride from 'method-override';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride());

const httpServer = http.createServer(sdk(app));

const port = process.env.PORT || 4000;

httpServer.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log('🚀 Server ready at http://localhost:' + port);
});

export default app;
