/**
* @api {get} /temperature/:idUser/:latitude/:longitude/:temperature/:humidity/:luminosity Alimenta informações para o servidor
* @apiName temperature
* @apiGroup Alimentacao
*
* @apiParam {Number} idUser ID do usuário.
* @apiParam {Number} latitude Latitude do dispositivo.
* @apiParam {Number} longitude Longitude do dispositivo.
* @apiParam {Number} temperature Temperatura atual.
* @apiParam {Number} humidity Umidade atual.
* @apiParam {Number} luminosity Luminosidade atual.
*
* @apiSuccess  String Identificador de status do servidor.
*/

/**
* @api {get} /temperature/list Lista as últimas informações agrupadas por usuário
* @apiName list
* @apiGroup Lista
*
*
* @apiSuccess  {Object[]} result Lista de informações.
*/

/**
* @api {get} /temperature/list/:idUser Lista as informações do usuário
* @apiName list idUser
* @apiGroup Lista
* @apiParam {Number} idUser ID do usuário.
*
*
* @apiSuccess  {Object[]} result Lista de informações do usuário.
*/

/**
* @api {get} /temperature/listAll Lista todas as informações de todos os usuários
* @apiName list all
* @apiGroup Lista
*
*
* @apiSuccess  {Object[]} result Lista de informações de todos os usuários.
*/
