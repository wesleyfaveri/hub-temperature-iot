import mongoose from 'mongoose';

const registerDb = (db = mongoose.connection, uri) => {
  db.on('error', console.error.bind(console, 'DB connection error:'));
  // eslint-disable-next-line no-console
  db.once('open', console.log.bind(console, '🚀 Database ready!'));
  return mongoose.connect(process.env.MONGODB_URI  || 'mongodb://localhost:27017/hub-temperature-iot', { useNewUrlParser: true });
};

export default registerDb;
