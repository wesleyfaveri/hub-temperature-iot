import mongoose from 'mongoose';

const defaultDefinition = {
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() }
};

export const createSchema = (definition) => new mongoose.Schema({ ...defaultDefinition, ...definition });
export const createModel = (name, schema) => mongoose.model(name, schema);
export const { Types } = mongoose.Schema;