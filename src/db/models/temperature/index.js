import { createSchema, createModel } from 'db/base';

const definition = {
  uuid: String,
  idUser: String,
  latitude: String,
  longitude: String,
  temperature: String,
  humidity: String,
  luminosity: String,
  date: Number,
};

const schema = createSchema(definition);

const TemperatureModel = createModel('Temperature', schema);

export default TemperatureModel;
